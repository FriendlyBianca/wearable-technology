import { exec } from 'child_process';

export async function getCreationTime(path: string) {
  return new Promise<number>((resolve, reject) => {
    exec(`git log --format=%at --follow "${path}"`, (error, stdout) => {
      if (error !== null) {
        reject(error);
        return;
      }
      const trimmed = stdout.trim();
      resolve(+trimmed.substr(trimmed.lastIndexOf('\n') + 1));
    });
  });
}
