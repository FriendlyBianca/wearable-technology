import { escapeKeyPressEvent } from '../input/keyboard';
import { author } from '../pages/author';
import { recentComments } from '../pages/recentComments';
import { recentMentions } from '../pages/recentMentions';
import { visitCount } from '../pages/visitCount';
import { wtcupVote } from '../pages/wtcupVote';
import { Content, newContent, Side } from './contentControl';
import { PathHandler } from './followQuery';
import { Layout, setLayout } from './layoutControl';
import { enterMenuMode } from './menuControl';

export interface Page {
  name: string;
  handler: (content: Content, path: string) => boolean;
}

const pages: Array<Page> = [
  recentComments,
  visitCount,
  recentMentions,
  author,
  wtcupVote,
];

export const pagePathHandler: PathHandler = path => {
  for (const page of pages) {
    if (path.startsWith(page.name)) {
      const content = newContent(Side.LEFT);
      escapeKeyPressEvent.onceUntil(enterMenuMode, content.leavingEvent);
      setLayout(Layout.MAIN);
      const handleResult = page.handler(content, path.substr(page.name.length));
      return handleResult;
    }
  }
  return false;
};
